(function(){
 var Phono = function(lpImg, audioEl){
 	var lp = lpImg;
 	var audio = audioEl;
 	var trackSrc = audio.children[0]; 
 	function playLp(e){
					trackSrc.setAttribute('src', 'sounds/' + e.target.getAttribute('alt') + '.mp3');
					audio.load();
					//e.target.classList.add("play");
					e.target.style.animationPlayState = "running";
					setTimeout(function(){ audio.play(); }, 1200);
				}
	function stopLp(e){
					audio.pause();
					e.target.style.animationPlayState = "paused";
					//e.target.classList.remove("play");
					trackSrc.setAttribute('src', '');
				};
	this.init = function(){
		for(i = 0; i < lp.length; i++){
			lp[i].addEventListener('mouseenter', playLp, false);
			lp[i].addEventListener('mouseleave', stopLp, false);
		}
	};
 };
 var myPhono = new Phono(document.querySelectorAll('.lp'), document.querySelector('audio'));
 myPhono.init();
})();






